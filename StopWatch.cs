﻿using GTA;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace StopWatch
{
    public class StopWatch_Script : Script
    {
        bool stopWatchRunning = false, backgroundEnabled;
        Stopwatch stopwatch;
        float backgroundPosX, backgroundPosY, textPosX, textPosY, backgroundWidth, backgroundHeight;
        Keys scriptToggleHoldKey, scriptTogglePressKey, stopwatchStartStopKey, stopwatchResetKey;
        int backgroundColor, textColor;

        public StopWatch_Script()
        {
            if (!File.Exists(Settings.Filename))
            {
                Settings.SetValue("BACKGROUND_POS_X", "POSITIONS", 40);
                Settings.SetValue("BACKGROUND_POS_Y", "POSITIONS", 40);
                Settings.SetValue("TEXT_POS_X", "POSITIONS", 45);
                Settings.SetValue("TEXT_POS_Y", "POSITIONS", 41);
                Settings.SetValue("BACKGROUND_WIDTH", "SIZES", 167);
                Settings.SetValue("BACKGROUND_HEIGHT", "SIZES", 30);
                Settings.SetValue("SCRIPT_TOGGLE_HOLD_KEY", "KEYS", Keys.RControlKey);
                Settings.SetValue("SCRIPT_TOGGLE_PRESS_KEY", "KEYS", Keys.S);
                Settings.SetValue("STOPWATCH_STARTSTOP_KEY", "KEYS", Keys.OemPeriod);
                Settings.SetValue("STOPWATCH_RESET_KEY_HOLD", "KEYS", Keys.Oemcomma);
                Settings.SetValue("BACKGROUND_ENABLED", "ENABLED", true);
                Settings.SetValue("BACKGROUND_COLOR", "COLORS", Color.Black.ToArgb());
                Settings.SetValue("TEXT_COLOR", "COLORS", Color.White.ToArgb());
                Settings.Save();
            }

            backgroundPosX = Settings.GetValueFloat("BACKGROUND_POS_X", "POSITIONS", 40);
            backgroundPosY = Settings.GetValueFloat("BACKGROUND_POS_Y", "POSITIONS", 40);
            textPosX = Settings.GetValueFloat("TEXT_POS_X", "POSITIONS", 45);
            textPosY = Settings.GetValueFloat("TEXT_POS_Y", "POSITIONS", 41);
            backgroundWidth = Settings.GetValueFloat("BACKGROUND_WIDTH", "SIZES", 167);
            backgroundHeight = Settings.GetValueFloat("BACKGROUND_HEIGHT", "SIZES", 30);
            scriptToggleHoldKey = Settings.GetValueKey("SCRIPT_TOGGLE_HOLD_KEY", "KEYS", Keys.RControlKey);
            scriptTogglePressKey = Settings.GetValueKey("SCRIPT_TOGGLE_PRESS_KEY", "KEYS", Keys.S);
            stopwatchStartStopKey = Settings.GetValueKey("STOPWATCH_STARTSTOP_KEY", "KEYS", Keys.OemPeriod);
            stopwatchResetKey = Settings.GetValueKey("STOPWATCH_RESET_KEY_HOLD", "KEYS", Keys.Oemcomma);
            backgroundEnabled = Settings.GetValueBool("BACKGROUND_ENABLED", "ENABLED", true);
            backgroundColor = Settings.GetValueInteger("BACKGROUND_COLOR", "COLORS", Color.Black.ToArgb());
            textColor = Settings.GetValueInteger("TEXT_COLOR", "COLORS", Color.White.ToArgb());

            stopwatch = new Stopwatch();
            KeyDown += StopWatch_Script_KeyDown;
            PerFrameDrawing += StopWatch_Script_PerFrameDrawing;
        }

        public void StopWatch_Script_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            if (isKeyPressed(scriptToggleHoldKey) && e.Key == scriptTogglePressKey)
            {
                stopWatchRunning = !stopWatchRunning;
                Subtitle((stopWatchRunning ? "Stopwatch enabled" : "Stopwatch disabled"));
            }

            if (stopWatchRunning)
            {
                if (!isKeyPressed(stopwatchResetKey) && e.Key == stopwatchStartStopKey)
                {
                    if (stopwatch.IsRunning)
                    {
                        stopwatch.Stop();
                    }
                    else
                    {
                        stopwatch.Start();
                    }
                }
                else if (isKeyPressed(stopwatchResetKey) && e.Key == stopwatchStartStopKey)
                {
                    stopwatch.Reset();
                }
            }

            /*if (e.Key == Keys.Add)
            {
                backgroundWidth++;
                Subtitle(backgroundWidth.ToString());
            }
            if (e.Key == Keys.Subtract)
            {
                backgroundWidth--;
                Subtitle(backgroundWidth.ToString());
            }*/
        }

        public void StopWatch_Script_PerFrameDrawing(object sender, GraphicsEventArgs e)
        {
            if (stopWatchRunning)
            {
                string endUserString = string.Format("{0}:{1:00}:{2:00}:{3:00}.{4:000}", stopwatch.Elapsed.Days, stopwatch.Elapsed.Hours, stopwatch.Elapsed.Minutes, stopwatch.Elapsed.Seconds, stopwatch.Elapsed.Milliseconds);

                if (backgroundEnabled)
                {
                    e.Graphics.DrawRectangle(new System.Drawing.RectangleF(backgroundPosX, backgroundPosY, backgroundWidth, backgroundHeight), Color.FromArgb(backgroundColor));
                }

                e.Graphics.DrawText(endUserString, textPosX, textPosY, Color.FromArgb(textColor));
            }
            else
            {
                if (stopwatch.IsRunning) stopwatch.Reset();
            }

            
        }

        public void Subtitle(string info, int millisecs = 1500)
        {
            GTA.Native.Function.Call("PRINT_STRING_WITH_LITERAL_STRING_NOW", "STRING", info, millisecs, 1);
        }
    }
}
