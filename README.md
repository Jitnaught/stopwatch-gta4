About mod:
The name pretty much says it all.

How to use it:
Press RCTRL + S to toggle stopwatch.
Press the period button to start/stop the stopwatch.
Press comma + period together (press comma first) to reset stopwatch.

What you can change in the ini file:
Position of where the text goes.
Position of where the background goes.
Size of the backgroud.
The keys.
Color of the text.
Color of the background.
Whether to show the background or not.

Also:
The ini file is created the first time the script is ran.
To change the colors you will need to get the ARGB integer for that color. Use the program 'ColorFinder.exe' in the zip file to help with that.

//Changelog
v1.1
Changed how the stopwatch looks.
Changed how the stopwatch works.
Added the reset key combo.
Added the option to change the color of the background.
Added the option to change the color of the text.
Added the option to change whether the background shows up or not.

Thanks to:
Aru: C++ Scripthook.
HazardX: .NET Scripthook.
twattyballs2011: The request.

How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with the cs file in this archive
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
